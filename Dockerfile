# syntax=docker/dockerfile:1

FROM debian:sid-slim

RUN rm -f /etc/apt/apt.conf.d/docker-clean; echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' > /etc/apt/apt.conf.d/keep-cache
RUN --mount=type=cache,target=/var/cache/apt,sharing=locked --mount=type=cache,target=/var/lib/apt,sharing=locked <<SCRIPT

set -euo pipefail

apt-get update
apt-get upgrade -q -y
apt-get install -q -y bat build-essential git fd-find fzf neovim ripgrep wget

# Create non-sudo user
useradd -l -u 1001 -G sudo -md /home/gitlab -s /bin/bash -p /home/gitlab gitlab
mkdir -p /home/gitlab-workspaces && chgrp -R 0 /home && chmod -R g=u /etc/passwd /etc/group /home

SCRIPT

ENV HOME="/home/gitlab"
ENV EDITOR="nvim"
ENV PAGER="less"

WORKDIR $HOME

USER 1001

RUN <<SCRIPT
# Rust install start
wget -q -O- --https-only --secure-protocol=TLSv1_2 https://sh.rustup.rs | sh -s -- -y --profile minimal
rustup component add rustfmt clippy cargo-fmt cargo-clippy
# Rust install end

# LazyVim install start

git clone https://github.com/LazyVim/starter ~/.config/nvim

rm -rf ~/.config/nvim/.git

cat <<EOF > ~/.config/nvim/lua/plugins/rustacean.lua
return {
  'mrcjkb/rustaceanvim',
  version = '^4', -- Recommended
  lazy = false, -- This plugin is already lazy
}
EOF

# Set fzf as file picker
echo 'vim.g.lazyvim_picker = "fzf"' >> ~/.config/nvim/lua/config/options.lua

# Preinstall all plugins for better developer experience.
nvim --headless '+Lazy! sync' '+TSUpdateSync' '+qa'

# LazyVim install end

# Helpful aliases
cat <<EOF >> ~/.bashrc
alias ls="ls --color=always"
alias ll="ls --color=always -l"
alias l="ls --color=always -lA"

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias mkdir='mkdir -p'

# Debian + Ubuntu sometimes fail to use
# the canonical names for tools. This
# fixes that issue by creating aliases.
alias fd='fdfind'
alias bat='batcat'
alias nv='nvim'
EOF

SCRIPT
