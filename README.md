# Standup thing

## Problem

1. Entering standup data into geekbot is tedious, time-consuming, and should be
   automated.

## Use case

- Get list of activities
    - MR comments
    - Issue comments
    - Creation of MR or issue
- Select an activity
- Paste in with a description and backticks for title and also enter url
- Repeat

## Requirements

- [ ] Fetch list of activities
- [ ] Allow user to select from this list
    - e.g.
        1. Commented on new UX for DS
- [ ] Prepare list
    1. Participated in `title` http://....
- [ ] Output the full list
- [ ] Copy/paste



