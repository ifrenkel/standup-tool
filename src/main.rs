use std::process::exit;

const METADATA: &str = r#"
{
    "version": "0.1.0",
    "description": "Make your daily stand up a breeze."
}
"#;

fn main() {
    let show_metadata = std::env::args().any(|arg| &arg == "--metadata");
    if show_metadata {
        println!("{}", METADATA);
        exit(0);
    }

    let events = pull_user_events();
    // TODO: properly handle errors
    println!("{}", events.unwrap())
}

fn pull_user_events() -> Option<String> {
    let args = ["user", "events", "-O", "json"];

    // TODO: properly handle errors
    let output = std::process::Command::new("glab")
        .args(args)
        .output()
        .expect("should not fail to get user events");

    // TODO: properly handle errors
    Some(String::from_utf8(output.stdout).unwrap())
}
